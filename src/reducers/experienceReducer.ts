import { Action } from "redux";
import { EXPERIENCES_FETCH } from "../actions/types";
import { IExperiences } from "../App";

interface IExperienceAction extends Action {
  payload: any;
}

export interface IExperienceReducer {
  experiences: IExperiences;
}

export const initialState: IExperienceReducer = {
  experiences: []
};

export default (state = initialState, action: IExperienceAction) => {
  switch (action.type) {
    case EXPERIENCES_FETCH:
      return { experiences: action.payload };
    default:
      return state;
  }
};
