import { Dispatch } from "redux";
import { IExperience } from "../App";
import { experiencesRef } from "../config/firebase";
import { EXPERIENCES_FETCH } from "./types";

export const addExperience = (dispatch: Dispatch) => async (
  newExperience: IExperience
) => {
  // tslint:disable-next-line
  console.log("Thunk to add", "actions");
  const data = await experiencesRef.doc(newExperience.title).set(newExperience);
  dispatch({
    payload: data,
    type: "EXPERIENCE_ADD"
  });
};

export const removeExperience = (experience: IExperience) => async (
  dispatch: Dispatch
) => {
  experiencesRef.doc(experience.title).delete();
};

export const fetchExperiences = (dispatch: Dispatch) => async () => {
  const snapshot = await experiencesRef.get();

  const experiences: IExperience[] = [];
  snapshot.forEach(doc =>
    experiences.push({ id: doc.id, title: doc.data().title })
  );

  dispatch({
    payload: experiences,
    type: EXPERIENCES_FETCH
  });
};
