import * as React from "react";
import * as ReactDOM from "react-dom";
import { App, IExperiences } from "./App";

const addExperience = jest.fn();
const fetchExperiences = jest.fn();
const experiences: IExperiences = [];

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <App
      addExperience={addExperience}
      experiences={experiences}
      fetchExperiences={fetchExperiences}
    />,
    div
  );
  ReactDOM.unmountComponentAtNode(div);
});
