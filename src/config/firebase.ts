import * as firebase from "firebase";
import 'firebase/firestore';
import * as keys from "./keys";
firebase.initializeApp(keys.FirebaseConfig);

const db = firebase.firestore();
db.settings({
  timestampsInSnapshots: true
});

export const experiencesRef = db.collection("experiences");
