import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import * as actions from "./actions";
import "./App.css";
import { IExperienceReducer } from "./reducers/experienceReducer";

export interface IExperience {
  id?: string;
  title: string;
}

export type IExperiences = IExperience[];

export interface IExperiencesState {
  experiences: IExperiences;
  currentExperience: string;
}

interface IExperienceDispatchProps {
  addExperience: (experience: IExperience) => Promise<any>;
  fetchExperiences: () => Promise<any>;
}

type ExperiencesProps = IExperienceDispatchProps & IExperienceReducer;

function immutablePush<T>(collection: T[], item: T) {
  return [...collection, item];
}

export class App extends React.Component<ExperiencesProps, IExperiencesState> {
  public state = { currentExperience: "", experiences: [] };

  public componentDidMount() {
    this.props.fetchExperiences();
  }

  public addExperience = (event: React.FormEvent<HTMLFormElement>): void => {
    event.preventDefault();
    this.setState(state => ({
      currentExperience: "",
      experiences: immutablePush(state.experiences, {
        title: this.state.currentExperience
      })
    }));
    this.props.addExperience({
      title: this.state.currentExperience
    });
  };

  public updateCurrentExperience = (
    event: React.FormEvent<HTMLInputElement>
  ): void => this.setState({ currentExperience: event.currentTarget.value });

  public render() {
    const experiences = [...this.state.experiences, ...this.props.experiences];

    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Experience tracker</h1>
        </header>
        <p className="App-intro">Save an experience</p>
        <form onSubmit={this.addExperience}>
          <input
            className="input-primary"
            type="text"
            value={this.state.currentExperience}
            onChange={this.updateCurrentExperience}
          />
          <button type="submit">Save</button>
        </form>
        <ul className="pa-0">
          {experiences.map((experience: IExperience, i: number) => (
            <li key={`${experience.title}- ${i}`}>{experience.title}</li>
          ))}
        </ul>
      </div>
    );
  }
}

const mapStateToProps = (state: any): IExperienceReducer => {
  return state.experience;
};

const mapDispatchToProps = (dispatch: Dispatch): IExperienceDispatchProps => ({
  addExperience: actions.addExperience(dispatch),
  fetchExperiences: actions.fetchExperiences(dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
